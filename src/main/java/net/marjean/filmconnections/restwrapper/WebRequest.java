package net.marjean.filmconnections.restwrapper;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@SuppressFBWarnings(value = {"EI_EXPOSE_REP", "EI_EXPOSE_REP2"}, justification = "Complaints about generated code.")
// NOTE: lombok has no plans to fix the way they generate getters and setters. https://github.com/projectlombok/lombok/issues/420
public class WebRequest {

    @Id
    private String url;
    @Column(columnDefinition = "TEXT")
    private String result;
    private LocalDateTime searchTime;

}

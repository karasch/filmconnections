package net.marjean.filmconnections.restwrapper;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@RequiredArgsConstructor
@Service
public class WebRequestCache {

    //NOTE: Package private for testing
    @Value("${rest.cache.expiry:#{null}}")
    Integer cacheExpiry;

    private final RequestRepository requestRepository;

    @SuppressFBWarnings(value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE", justification = "I am checking for null you insensitive clod!")
    public String cachedRequest(String requestURL) throws IOException {
        // check the cache
        Optional<WebRequest> optionalWebRequest = requestRepository.findById(requestURL);

        if(optionalWebRequest.isPresent()){
            WebRequest cachedRequest = optionalWebRequest.get();

            // cache hit
            log.debug("Found request in cache, cached from {}", cachedRequest.getSearchTime());
            //// check timestamp for validity
            if(cacheExpiry != null && cacheExpiry > 0){
                if(cachedRequest.getSearchTime().isAfter(LocalDateTime.now().minusMinutes(cacheExpiry))){
                    log.debug("Returning cached response");
                    return cachedRequest.getResult();
                }
                else{
                    log.debug("Cached response too old.  Sending new request.");
                }
            }
            else{
                log.debug("Cache expiry disabled, returning cached response.");
                return cachedRequest.getResult();
            }

        }

        // cache miss
        log.debug("Cache miss");
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();

        Request request = new Request.Builder()
                .url(requestURL)
                .method("GET", null)
                .build();

        try (Response response = client.newCall(request).execute()) {

            String responseBody = "";

            //Hey look SpotBugs, there's a check for null!
            if (response.isSuccessful() && response.body() != null) {

                responseBody = response.body().string();

                //Check that we weren't passed an error stealthily.
                JSONObject jsonObject = new JSONObject(responseBody);

                //if(errorMessage == null || errorMessage.isEmpty()) {
                if(jsonObject.has("errorMessage") && !jsonObject.isNull("errorMessage")){
                    // Aw crap, the service isn't playing nice.
                    log.error("Received errorMessage from service: {}", jsonObject.getString("errorMessage"));
                    //TODO should we 501?
                }
                else{
                    // We did not receive an error message, cleared to proceed.
                    WebRequest cachedCopy = new WebRequest();
                    cachedCopy.setUrl(requestURL);
                    cachedCopy.setResult(responseBody);
                    cachedCopy.setSearchTime(LocalDateTime.now());

                    requestRepository.save(cachedCopy);
                }
            } else {
                log.error("HTTP Status: {}", response.code());
                log.error("Status message: {}", response.message());
            }

            return responseBody;
        }
    }
}

package net.marjean.filmconnections.restwrapper;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@ConditionalOnProperty(name = "allowed.cors.origin")
@Configuration
@Slf4j
public class RestWrapperWebMvcConfigurer implements WebMvcConfigurer {

    @Value("${allowed.cors.origin}")
    private String allowedCorsOrigin;

    @Override
    public void addCorsMappings(@NotNull CorsRegistry registry) {
        WebMvcConfigurer.super.addCorsMappings(registry);

        log.info("Allowing CORS access from {}", allowedCorsOrigin);

        registry.addMapping("/search/**").allowedOrigins(allowedCorsOrigin);
        registry.addMapping("/title/**").allowedOrigins(allowedCorsOrigin);

    }


}

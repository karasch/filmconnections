package net.marjean.filmconnections.restwrapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebInputException;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RestwrapperController {

    @Value("${rest.api.key}")
    private String apiKey;

    private final WebRequestCache webRequestCache;

    private final String baseURL = "https://imdb-api.com/en/API/";

    private final String[] validSearchArray = {
            "Search",
            "SearchTitle",
            "SearchMovie",
            "SearchSeries",
            "SearchName"
    };

    private final Set<String> validSearchTypes = new HashSet<>(Arrays.asList(validSearchArray));

    @GetMapping(value = "/search/{searchType}/{searchTerm}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String searchMovie(@PathVariable String searchType, @PathVariable String searchTerm) throws IOException {

        // Check that the searchType we were sent is valid
        if(!validSearchTypes.contains(searchType)){
            throw new ServerWebInputException( searchType + " is not a valid search type.");
        }
        //https://imdb-api.com/en/API/Search/<API Key>/inception 2010

        String requestURL = baseURL + searchType + "/" + apiKey + "/" + searchTerm;

        return webRequestCache.cachedRequest(requestURL);
    }


    @GetMapping(value = {"/title/{titleKey}/{arguments}", "/title/{titleKey}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getTitle(@PathVariable String titleKey, @PathVariable Optional<String> arguments) throws IOException {

        //https://imdb-api.com/en/API/Title/<API Key>/tt0110413

        String serviceArguments;
        if(arguments.isPresent())
            serviceArguments = arguments.get();
        else
            serviceArguments = "FullCast";

        String titleType = "Title/";
        String requestURL = baseURL + titleType + apiKey + "/" + titleKey + "/" + serviceArguments;

        return webRequestCache.cachedRequest(requestURL);
    }



}

package net.marjean.filmconnections.restwrapper;

import org.springframework.data.repository.CrudRepository;

public interface RequestRepository extends CrudRepository<WebRequest, String> {
}

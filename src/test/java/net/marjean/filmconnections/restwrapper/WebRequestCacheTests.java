package net.marjean.filmconnections.restwrapper;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class WebRequestCacheTests {

    private MockWebServer mockWebServer;

    private static RequestRepository mockRepository;

    private static WebRequestCache testUnit;

    @BeforeAll
    public static void initialize(){
        mockRepository = Mockito.mock(RequestRepository.class);
        testUnit = new WebRequestCache(mockRepository);
    }

    @BeforeEach
    public void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        Mockito.reset(mockRepository);
    }

    @AfterEach
    public void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    public void testCachedRequest_noExpiryCacheMiss(){
        String message = "{\"id\": 1, \"errorMessage\": \"\"}";
        MockResponse mockedResponse = new MockResponse()
                .setBody(message) //Sample
                .addHeader("Content-Type", "application/json");
        mockWebServer.enqueue(mockedResponse);

        String url = mockWebServer.url("/request").toString();

        String result = "";
        try {
            result = testUnit.cachedRequest(url);
        } catch (IOException e) {
            fail();
        }

        //verify that we checked the cache
        Mockito.verify(mockRepository).findById(url);
        //Verify that what we got back is what we expected
        assertEquals(message, result);

    }

    @Test
    public void testCachedRequest_noExpiryCacheHit(){
        String URL = "http://fake.url";
        String cachedResponse = "FAKE RESPONSE";

        WebRequest wr = new WebRequest();
        wr.setUrl(URL);
        wr.setResult(cachedResponse);
        wr.setSearchTime(LocalDateTime.now());

        Mockito.when(mockRepository.findById(URL)).thenReturn(Optional.of(wr));

        try {
            String response = testUnit.cachedRequest(URL);

            assertEquals(cachedResponse, response);

            Mockito.verify(mockRepository).findById(URL);
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    public void testCachedRequest_cacheHit(){
        Integer cacheExpiry = testUnit.cacheExpiry;
        testUnit.cacheExpiry = 24*60;

        String URL = "http://fake.url";
        String cachedResponse = "FAKE RESPONSE";

        WebRequest wr = new WebRequest();
        wr.setUrl(URL);
        wr.setResult(cachedResponse);
        wr.setSearchTime(LocalDateTime.now());

        Mockito.when(mockRepository.findById(URL)).thenReturn(Optional.of(wr));

        try {
            String response = testUnit.cachedRequest(URL);

            assertEquals(cachedResponse, response);

            Mockito.verify(mockRepository).findById(URL);
        } catch (IOException e) {
            fail();
        }

        // Set things back the way they were
        testUnit.cacheExpiry = cacheExpiry;
    }

    @Test
    public void testCachedRequest_expiredResult(){
        Integer cacheExpiry = testUnit.cacheExpiry;
        testUnit.cacheExpiry = 5;

        String message = "{\"id\": 1, \"errorMessage\": \"\"}";
        MockResponse mockedResponse = new MockResponse()
                .setBody(message) //Sample
                .addHeader("Content-Type", "application/json");
        mockWebServer.enqueue(mockedResponse);

        String url = mockWebServer.url("/request").toString();

        WebRequest wr = new WebRequest();
        wr.setUrl(url);
        wr.setResult("NOPE");
        wr.setSearchTime(LocalDateTime.now().minusMinutes(6));

        Mockito.when(mockRepository.findById(url)).thenReturn(Optional.of(wr));

        try {
            String response = testUnit.cachedRequest(url);
            Mockito.verify(mockRepository).findById(url);
            assertEquals(message, response);
        } catch (IOException e) {
            fail();
        }


        // Set things back the way they were
        testUnit.cacheExpiry = cacheExpiry;
    }

    @Test
    public void testCachedResponse_webServiceFail(){
        MockResponse mockResponse = new MockResponse();
        mockResponse.setBody("hi");
        mockResponse.setResponseCode(404);

        mockWebServer.enqueue(mockResponse);

        String url = mockWebServer.url("/request").toString();

        try {
            String result = testUnit.cachedRequest(url);
            assertEquals("", result);
        } catch (IOException e) {
            fail();
        }
    }
}

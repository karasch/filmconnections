package net.marjean.filmconnections.restwrapper;

import com.sun.org.apache.xpath.internal.Arg;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest
public class RestwrapperControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WebRequestCache webRequestCache;

    @Test
    public void testSearchEndpoint(){


        try {
            mockMvc.perform(get("/search/Search/inception 2010")).andDo(print()).andExpect(status().isOk());

        } catch (Exception e) {
            fail();
        }

    }

    @Test
    public void testInvalidSearchType(){
        try {
            mockMvc.perform(get("/search/s/inception 2010")).andDo(print()).andExpect(status().is(400));
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testTitleEndpoint_noOptionalArgument(){

        String fullURL = "https://imdb-api.com/en/API/Title/testkey/tt0110413/FullCast";
        try {
            ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);

            mockMvc.perform(get("/title/tt0110413")).andDo(print()).andExpect(status().isOk());
            Mockito.verify(webRequestCache).cachedRequest(urlCaptor.capture());

            assertEquals(fullURL, urlCaptor.getValue());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testTitleEndpoint_withOptionalArgument(){

        String testArgument = "RandomString";

        String fullURL = "https://imdb-api.com/en/API/Title/testkey/tt0110413/" + testArgument;

        try {
            ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);
            mockMvc.perform(get("/title/tt0110413/" + testArgument)).andDo(print()).andExpect(status().isOk());

            Mockito.verify(webRequestCache).cachedRequest(urlCaptor.capture());

            assertEquals(fullURL, urlCaptor.getValue());
        } catch (Exception e) {
            fail();
        }
    }

}

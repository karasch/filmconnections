# Film Connections backend

A service to wrap and cache imdb-ish web services

Currently using https://imdb-api.com/

I have written a UI based on these services which is deployed to https://filmconnections.herokuapp.com/

The code for the UI is available at https://gitlab.com/karasch/filmconnections-ui

## Required properties

* `rest.api.key` The required API key for the services we need to access.  Don't check this into git.

## Optional Properties

* `rest.cache.expiry` Minutes until cached responses are considered too old. (Defaults to infinity)
* `allowed.cors.origin` CORS requests will be accepted from the value of this property.  Useful for React development when set to http://localhost:3000

## Recommended Test Properties

* `spring.datasource.url` No point in spamming the underlying services with calls
* `spring.jpa.hibernate.ddl-auto` Set to `update` at least until I'm sure the schema is staying put
* `spring.h2.console.enabled` Set to `true` Because who doesn't love a simple web-based SQL client for testing

## Implemented Endpoints

* http://localhost:8080/search/Search/inception%202010
* http://localhost:8080/title/tt0110413

## TODO

* Implement actor calls
* Improve error handling - imdb-api doesn't return an error condition, it just puts the error in the body.

## Project setup

### Gradle options
* `./gradlew bootRun -Pprofiles=local`
